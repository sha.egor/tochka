# Tochka Dostupa (tochka)

Vasha Tochka Dostupa

## Install the dependencies
```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Lint the files
```bash
yarn lint
# or
npm run lint
```



### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).

https://zoom.cnews.ru/publication/item/64607


https://developer.android.com/studio/publish/app-signing

https://capacitorjs.com/docs/apis/splash-screen

https://capacitorjs.com/docs/guides/splash-screens-and-icons

https://capacitorjs.com/docs/vscode/getting-started


https://global.developer.mi.com/register/profile?devType=individual&from=result

https://web.dev/i18n/ru/customize-install/

https://dev.to/konyu/using-javascript-to-determine-whether-the-client-is-ios-or-android-4i1j

```js
export const checkIphone = () => {
  const u = navigator.userAgent
  return !!u.match(/iPhone/i)
}
export const checkAndroid = () => {
  const u = navigator.userAgent
  return !!u.match(/Android/i)
}
export const checkIpad = () => {
  const u = navigator.userAgent
  return !!u.match(/iPad/i)
}
export const checkMobile = () => {
  const u = navigator.userAgent
  return !!u.match(/Android/i) || !!u.match(/iPhone/i)
}
export default {
  checkIphone,
  checkAndroid,
  checkIpad,
  checkMobile,
}
```
